hash_bitmask = 0x7FFF
charmap = {7231:"0", 27606:"1", 3062:"2", 10199:"3", 22215:"4", 11605:"5", 14280:"6", 19402:"7", 2638:"8", 15881:"9", 5927:"A", 19685:"B", 27737:"C", 1216:"D", 7803:"E", 21594:"F", 16174:"G", 26393:"H", 28180:"I", 18108:"I", 2137:"I", 16187:"I", 27794:"I", 20127:"I", 28038:"I", 7913:"J", 9947:"J", 8598:"J", 10903:"J", 22555:"J", 6475:"J", 1414:"K", 20853:"L", 29155:"M", 4487:"N", 19281:"O", 10406:"P", 3152:"Q", 22646:"R", 1900:"S", 22893:"T", 20237:"U", 20473:"V", 29250:"W", 26215:"X", 8396:"Y", 22685:"Z"}
charwidth = {'9': 11, 'R': 11, 'Q': 14, 'C': 12, 'X': 11, 'B': 10, 'D': 13, 'F': 9, '6': 11, 'I': 4, 'L': 9, '4': 11, 'P': 10, 'A': 12, 'Z': 11, '1': 11, '7': 11, 'N': 13, 'J': 5, '5': 11, 'O': 14, 'M': 16, 'Y': 11, '8': 11, 'W': 16, 'H': 13, '3': 11, 'K': 12, 'S': 9, 'T': 11, 'G': 13, 'U': 12, '2': 11, 'V': 12, '0': 11, 'E': 9}

def match(image):
	w,h = image.size
	pix = image.load()
	out = ''
	start = 4
	while start < w:
		start += 1
		hh = 0x811c9dc5
		for x in range(4):
			for y in range(20):
				nx = int(start+x-0.2*(y+4))
				hh ^= pix[nx,y+4][3] if nx<w else 0
				hh *= 16777619
				hh &= hash_bitmask
		if hh in charmap:
			char = charmap[hh]
			out += char
			start += charwidth[char]
	if len(out) == 4:
		return out
	return None

if __name__ == '__main__':
	from timeit import default_timer as timer
	from PIL import Image
	test_files = ['data/%04d.png' % i for i in range(10000)]
	test_img = []
	for file in test_files:
		im = Image.open(file)
		im.load()
		test_img.append(im)

	print('Testing...')
	count = 0
	start = timer()
	for im in test_img:
		if match(im) is not None:
			count += 1
	end = timer()
	print('Execution time: %.3f ms/image' % ((end-start)*1000/len(test_files)))
	print('Accuracy: %.2f%%' % (count*100/len(test_files)))