from PIL import Image
from os.path import join
import os
import hashlib

data_dir = 'data'
out_dir = 'clust'
files = ['%04d.png'%i for i in range(3000)]

try:
    os.makedirs(out_dir)
except OSError as e:
    pass

bucket = set()
def hash(img):
   return hashlib.md5(img.tobytes()).hexdigest()

for file in files:
	image = Image.open(join(data_dir,file))
	imx = Image.new('RGBA', image.size, (255,255,255,255))
	w,h = image.size
	pix = image.load()
	pixd = imx.load()
	for x in range(w):
		for y in range(h):
			nx = int(x-0.2*y)
			if nx >= 0:
				pixd[x,y] = pix[nx,y]
	thumb = imx.crop((5,4,9,24))
	hh = hash(thumb)[:4]
	if hh not in bucket:
		bucket.add(hh)
		thumb.save(join(out_dir, hh+'.png'))
		imx.save(join(out_dir, hh+'_full.png'))
		print(hh)
print(len(bucket))