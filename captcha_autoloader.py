from pyquery import PyQuery as pq
import requests
import os

login_url = "https://www2.reg.chula.ac.th/servlet/com.dtm.chula.reg.servlet.InitLogonServlet"
base_url = "https://www2.reg.chula.ac.th/servlet/"

sess = requests.session()
def fetch(fn):
	resp = sess.get(login_url, verify=False)
	if resp.status_code != 200:
		raise Exception("Can't get login page")
	dom = pq(resp.text)
	img = dom('[id=CAPTCHA]')
	img_url = img.attr('src')
	resp = sess.get(base_url+img_url, verify=False)
	if resp.status_code != 200:
		raise Exception("Can't get image")
	with open(fn, 'wb') as fp:
		fp.write(resp.content)

if __name__ == "__main__":
	try:
	    os.makedirs('data')
	except OSError as e:
	    pass
	for i in range(10000):
		fetch('data/%04d.png' % i)