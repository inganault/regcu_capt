from PIL import Image
import sys
from collections import defaultdict
import os

files = ['data/%04d.png'%i for i in range(10000)]
out_dir = 'clust2'
char_dir = 'character'

clss = [i.split('.')[0].upper() for i in os.listdir(char_dir)]

# clss_skip = {}
clss_skip = {'I': 4, 'Q': 14, 'X': 11, '0': 11, 'U': 12, 'S': 9, 'D': 13, 'H': 13, 'J': 5, '4': 11, 'L': 9, 'N': 13, '2': 11, 'R': 11, '7': 11, 'B': 10, 'W': 16, 'E': 9, '3': 11, 'Y': 11, 'T': 11, 'F': 9, 'A': 12, 'C': 12, 'P': 10, 'M': 16, '1': 11, 'V': 12, 'K': 12, 'O': 14, '9': 11, '8': 11, '6': 11, 'G': 13, 'Z': 11, '5': 11}
error_check_enable = 0
print_all_file = 1

clss_file = {i:Image.open(os.path.join(char_dir, '%s.png'%i)).load() for i in clss}

def hash(im):
	hh = 0x811c9dc5
	for x in range(4):
		for y in range(20):
			hh ^= im[x,y][3]
			hh *= 16777619
			hh &= 0xFFFFFFFF
	return hh

clss_hash = {i:hash(clss_file[i]) for i in clss}
hash_mask = 0x1
while len({i&hash_mask for i in clss_hash.values()}) < len(clss):
	if hash_mask >= 0xFFFFFFFF:
		print('Hash collision')
		sys.exit(1)
	hash_mask = (hash_mask << 1) | 1
print('Initial hash bitmask: %08X'%hash_mask)

char_width = {i:defaultdict(int) for i in {i[0] for i in clss}}

count = 0
for file in files:
	image = Image.open(file)
	w,h = image.size
	pix = image.load()
	out = ''
	last_match = 0
	errors = []

	start = 4
	while start < w:
		start += 1

		hh = 0x811c9dc5
		for x in range(4):
			for y in range(24-4):
				nx = int(start+x-0.2*(y+4))
				px = pix[nx,y+4][3] if nx<w else 0
				hh ^= px
				hh *= 16777619
				hh &= 0xFFFFFFFF
		error_min = (999, '')
		for c in clss:
			if error_check_enable or hh&hash_mask == clss_hash[c]&hash_mask: # Brute force check
				error = 0
				for x in range(4):
					for y in range(24-4):
						nx = int(start+x-0.2*(y+4))
						px = pix[nx,y+4][3] if nx<w else 0
						error += clss_file[c][x,y][3] != px
				if error < error_min[0]:
					error_min = (error, c[0], start)

			if hh&hash_mask == clss_hash[c]&hash_mask: # Hash check
				if error:
					while hh&hash_mask == clss_hash[c]&hash_mask: # Find minimum essential bitmask
						if hash_mask >= 0xFFFFFFFF:
							print('Hash collision')
							sys.exit(1)
						hash_mask = (hash_mask << 1) | 1
					if not error_check_enable:
						break
				else:
					if last_match:
						char_width[out[-1]][start-last_match] += 1
					out += c[0]
					last_match = start
					error_min = (0, c[0], start)

					if c[0] in clss_skip:
						start += clss_skip[c[0]]
					break
		if error_min[0] != 0:
			errors.append(error_min)
	if print_all_file:
		print(file,out)
	if len(out) == 4:
		count +=1
	elif len(out) > 4:
		print('Wrong')
		break
	elif len(out) == 3:
		print(file,out,file=sys.stderr)
		print([i[:2] for i in sorted(errors)[:4]])

		if error_check_enable:
			error_min = min(errors)
			try:
			    os.makedirs(out_dir)
			except OSError as e:
			    pass

			start = error_min[2]
			imx = Image.new('RGBA', (w+4-start,20), (255,255,255,255))
			nw,nh = imx.size
			pix = image.load()
			pixd = imx.load()
			for x in range(nw):
				for y in range(nh):
					nx = int(start+x-0.2*(y+4))
					pixd[x,y] = pix[nx,y+4] if nx<w else (0,0,0,0)
			thumb = imx.crop((0,0,4,20))

			hh = hash(thumb.load())
			idx = error_min[1] + '_%04X'%(hh&0xFFFF)
			thumb.save(os.path.join(out_dir, idx+'.png'))
			imx.save(os.path.join(out_dir, idx+'_full.png'))
			print('Saved at',idx)

			clss.append(idx)
			clss_hash[idx] = hh
			clss_file[idx] = thumb.load()
	else :
		print(file,out,file=sys.stderr)
		print([i[:2] for i in sorted(errors)[:4]])

print('Character width:')
for i in sorted(char_width):
	print('>',i,dict(char_width[i]))
print('Accuracy: %.2f%%'%(100*count/len(files)))
print('Hash bitmask: 0x%X'%hash_mask)
print(', '.join('0x%X:"%c"'%(clss_hash[i]&hash_mask,i[0]) for i in clss_file))
print('Skip:', {i:min(char_width[i].keys())-1 if len(char_width[i]) else 0 for i in char_width})