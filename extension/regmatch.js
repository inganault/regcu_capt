(function(){
	var img = document.getElementById('CAPTCHA');

	var detect_captcha = function(){
		var hash_bitmask = 0x7FFF;
		var charmap = {7231:"0", 27606:"1", 3062:"2", 10199:"3", 22215:"4", 11605:"5", 14280:"6", 19402:"7", 2638:"8", 15881:"9", 5927:"A", 19685:"B", 27737:"C", 1216:"D", 7803:"E", 21594:"F", 16174:"G", 26393:"H", 28180:"I", 18108:"I", 2137:"I", 16187:"I", 27794:"I", 20127:"I", 28038:"I", 7913:"J", 9947:"J", 8598:"J", 10903:"J", 22555:"J", 6475:"J", 1414:"K", 20853:"L", 29155:"M", 4487:"N", 19281:"O", 10406:"P", 3152:"Q", 22646:"R", 1900:"S", 22893:"T", 20237:"U", 20473:"V", 29250:"W", 26215:"X", 8396:"Y", 22685:"Z"};
		var charwidth = {'9': 11, 'R': 11, 'Q': 14, 'C': 12, 'X': 11, 'B': 10, 'D': 13, 'F': 9, '6': 11, 'I': 4, 'L': 9, '4': 11, 'P': 10, 'A': 12, 'Z': 11, '1': 11, '7': 11, 'N': 13, 'J': 5, '5': 11, 'O': 14, 'M': 16, 'Y': 11, '8': 11, 'W': 16, 'H': 13, '3': 11, 'K': 12, 'S': 9, 'T': 11, 'G': 13, 'U': 12, '2': 11, 'V': 12, '0': 11, 'E': 9};
		console.log('Detecting captcha...');
		if(img.height!=24){
			console.warn('Wrong captcha image size');
			return;
		}
		var imw = img.width;
		var canvas = document.createElement('canvas');
		canvas.width = img.width;
		canvas.height = img.height;
		canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
		var imdata = canvas.getContext('2d').getImageData(0, 0, img.width, img.height).data;
		var out = '';
		for(var start = 5; start < imw; ++start){
			var h = 0x811c9dc5;
			for(var x = 0; x < 4; ++x){
				for(var y = 4; y < 24; ++y){
					new_x = (start+x-0.2*y) | 0; // skew
					h ^= new_x<imw ? imdata[(y*imw+new_x)*4+3] : 0;
					h = (h + (h << 1) + (h << 4) + (h << 7) + (h << 8) + (h << 24));
				}
			}
			h &= hash_bitmask;
			if(h in charmap){
				out += charmap[h];
				start += charwidth[charmap[h]];
			}
		}
		if(out.length == 4){
			document.getElementsByName('code')[0].value = out;
			console.info("Captcha =",out);
		}else
			console.info("Can't read captcha");
	};

	if (img.complete){
		detect_captcha();
	}else
		img.addEventListener('load', detect_captcha);

})();